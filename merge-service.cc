#include "merge-service.hh"
#include <seastar/core/smp.hh>
#include <seastar/core/seastar.hh>
#include <seastar/core/sleep.hh>
#include <seastar/core/memory.hh>   
#include <iostream>
#include <filesystem>
#include <memory>

merge_service::merge_service(uint64_t block_size, size_t file_buffer_size, size_t file_buffer_alignment)
        : _block_size(block_size), _file_buffer_size(file_buffer_size), _file_buffer_alignment(file_buffer_alignment)
        , _block_list([block_size](const block_data& a, const block_data& b){return std::memcmp(a._block.get(), b._block.get(), block_size) > 0;}){
    _files_data.reserve(16);
    _file_chunk_size = (file_buffer_size/block_size)*_block_size;
    _min_block_count = file_buffer_size/block_size;
}

seastar::future<> merge_service::init(seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > > files){
    _files_data.resize((*files.get()).size());
    _files = std::move(files);
    if(_files_data.size() > 0) {
        _min_block_count = std::max(_file_buffer_size/_block_size, (seastar::memory::free_memory() - (seastar::memory::min_free_memory()) - _files_data.size()*_file_buffer_size)/(_block_size*_files_data.size()));
    }
    return seastar::do_for_each(boost::make_counting_iterator<size_t>(0), boost::make_counting_iterator<size_t>(std::min(_files_data.size(), (*_files.get()).size())), [this](auto idx){
        return seastar::open_file_dma((*_files.get())[idx], seastar::open_flags::ro).then([this, idx](seastar::file f){
            _files_data[idx]._file_name = (*_files.get())[idx];
            _files_data[idx]._file = f;
            return f.size().then([this, idx](auto size){
                _files_data[idx]._file_size = size;
                return seastar::make_ready_future<>();    
            });
        });
    });
}

seastar::future<> merge_service::run(){
    return seastar::do_with(size_t(0), [this](size_t& finished){
        return seastar::do_until([this, &finished]{return finished == _files_data.size();}, [this, &finished]{
            finished = 0;
            return seastar::parallel_for_each(_files_data, [this, &finished](file_data& fd){
                if(fd._read_pos >= fd._file_size){
                    ++finished;
                }
                if(fd._read_pos < fd._file_size && fd._data.size() <= _min_block_count){
                    return seastar::do_with(seastar::temporary_buffer<char>::aligned(_file_buffer_alignment, _file_buffer_size), [this, &fd](auto& buf) {
                        return fd._file.dma_read(fd._read_pos, buf.get_write(), _file_chunk_size).then([this, &fd, &buf](size_t count){
                            return seastar::do_with(uint64_t(0),[this, &fd, &buf, count](auto& chunk_pos){
                                return seastar::do_until([this, &chunk_pos, count, &fd]{return (chunk_pos + _block_size) > count;}, [this, &fd, &chunk_pos, count, &buf]{
                                    std::unique_ptr<char[]> d(new char[_block_size]);
                                    std::memcpy(d.get(), buf.get()+chunk_pos, _block_size);
                                    fd._data.push_back(std::move(d));
                                    chunk_pos += _block_size;
                                    return seastar::make_ready_future<>(); 
                                }).then([&fd, count]{
                                    fd._read_pos += count;
                                });
                            });
                        });
                    });
                }
                return seastar::make_ready_future<>();
            }).then([]{
                using namespace std::chrono_literals;
                return seastar::sleep(1ns);
            });
        });
    });
    return seastar::make_ready_future<>();
}

seastar::future<> merge_service::merge(){
    return seastar::do_for_each(boost::make_counting_iterator<size_t>(0), boost::make_counting_iterator<size_t>(_files_data.size()), [this](size_t file_idx){
        return get_next_block(file_idx).then([this, file_idx](auto data_ptr){
            if(data_ptr){
                _block_list.push({file_idx, std::move(data_ptr)});
            }
        });
        return seastar::make_ready_future<>();
    }).then([this]{
        if(_files_data.size()>0){
            return seastar::with_file(seastar::open_file_dma("./tmp_dir/m_" + std::to_string(seastar::this_shard_id()) + "_" + std::to_string(_output_file_count) + ".tmp", seastar::open_flags::rw | seastar::open_flags::create | seastar::open_flags::truncate), [this](seastar::file& f){
                return seastar::do_with(std::make_unique<char[]>(_file_buffer_size), uint64_t(0), uint64_t(0), uint64_t(0), [this, &f](auto& buf, auto& chunk_pos, auto& write_pos, auto& file_idx){
                    return seastar::do_until([this]{return _block_list.empty();}, [this, &f, &buf, &chunk_pos, &write_pos, &file_idx]{
                        auto& d = _block_list.top();
                        file_idx = d._file_idx;
                        std::memcpy(buf.get()+chunk_pos, d._block.get(), _block_size);
                        _block_list.pop();
                        chunk_pos += _block_size;
                        return get_next_block(file_idx).then([this, &f, &buf, &chunk_pos, &write_pos, &file_idx](auto data_ptr){
                            if(data_ptr){
                                _block_list.push({file_idx, std::move(data_ptr)});
                            }
                            return seastar::make_ready_future<>();
                        }).then([this, &f, &buf, &chunk_pos, &write_pos]{
                            if(chunk_pos > (_file_buffer_size-_block_size))
                            {
                                return f.dma_write(write_pos, buf.get(), _file_chunk_size).then([this, &chunk_pos, &write_pos](size_t count) {
                                    chunk_pos = 0;
                                    write_pos += count;
                                    return seastar::make_ready_future<>();
                                });
                            }
                            return seastar::make_ready_future<>();
                        });
                    }).then([this, &f, &buf, &chunk_pos, &write_pos]{
                        if(chunk_pos > 0)
                        {
                            return f.dma_write(write_pos, buf.get(), chunk_pos).then([](size_t count){
                                return seastar::make_ready_future<>();
                            });
                        }
                        return seastar::make_ready_future<>();
                    });
                });
            }).then([this]{
                _output_file_count++;
                return clear();
            });
        }
        return seastar::make_ready_future<>();
    });
    return seastar::make_ready_future<>();
}

seastar::future<> merge_service::clear(){
    return seastar::do_for_each(boost::make_counting_iterator<size_t>(0), boost::make_counting_iterator<size_t>(_files_data.size()), [this](auto idx){
        return _files_data[idx]._file.close().then([this, idx]{
            return seastar::remove_file((*_files.get())[idx]);
        });
    }).then([this]{
        _files_data.clear();
        _files.reset();
        return seastar::make_ready_future<>();
    });
}

seastar::future<std::unique_ptr<char[]> > merge_service::get_next_block(size_t idx){
    return seastar::do_until([this, idx]{return (!_files_data[idx]._data.empty()) || (_files_data[idx]._read_pos >= _files_data[idx]._file_size && _files_data[idx]._data.empty()); }, [this, idx]{
        using namespace std::chrono_literals;
        return seastar::sleep(1ns);
    }).then([this, idx]{
        if(!_files_data[idx]._data.empty()){
            auto ret = std::unique_ptr<char[]> (std::move(_files_data[idx]._data.front()));
            _files_data[idx]._data.pop_front();
            return seastar::make_ready_future<std::unique_ptr<char[]> >(std::move(ret));
        }
        return seastar::make_ready_future<std::unique_ptr<char[]> >(std::unique_ptr<char[]>());
    });
}

seastar::future<> merge_service::stop(){
    return seastar::make_ready_future<>();
}

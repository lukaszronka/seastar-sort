#include "merge-manager.hh"
#include <seastar/core/seastar.hh>
#include <seastar/core/memory.hh>
#include <algorithm>

merge_manager::merge_manager(std::string& tmp_dir_name, uint64_t block_size, size_t file_buffer_size, size_t file_buffer_alignment)
        : _tmp_dir_name(tmp_dir_name), _block_size(block_size), _file_buffer_size(file_buffer_size), _file_buffer_alignment(file_buffer_alignment) {
    _file_chunk_size = (file_buffer_size/block_size)*block_size;
 }

seastar::future<> merge_manager::prepare_file_list(){
    std::filesystem::path tmp(_tmp_dir_name);
    _splitted_files.clear();
    _file_count = std::distance(std::filesystem::directory_iterator{tmp}, std::filesystem::directory_iterator{});
    size_t max_files_per_service = std::min((size_t)16, (seastar::memory::free_memory() - seastar::memory::min_free_memory())/(_file_buffer_size*2));
    _files_per_service = std::max(2, (int)std::min(max_files_per_service, size_t(_file_count/seastar::smp::count+(_file_count%seastar::smp::count>0))));
    _file_count = 0;
    _splitted_files.push_back(std::vector<std::string> {});
    for(auto& file: std::filesystem::directory_iterator(tmp)){
        if(_splitted_files.back().size() == _files_per_service){
            _splitted_files.push_back(std::vector<std::string> {});
        }
        _splitted_files.back().push_back(file.path());
        _file_count++;
    }
    if(_splitted_files.back().size() == 1 && _splitted_files.size() > 1){
        auto last = _splitted_files.back();
        _splitted_files.pop_back();
        _splitted_files.back().push_back(last[0]);
    }
    return seastar::make_ready_future<>();
}

std::string merge_manager::get_last_file(){
    return _splitted_files.front()[0];
}

size_t merge_manager::file_count(){
    return _file_count;
}

seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > > merge_manager::get_files_per_merger(){
    if(!_splitted_files.empty())
    {
        auto ret = seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > >(std::make_unique<std::vector<std::string> >(std::vector<std::string>(_splitted_files.front())));
        _splitted_files.pop_front();
        _file_count -= ret.get()->size();
        return ret;
    }
    return seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > >(std::make_unique<std::vector<std::string> >(std::vector<std::string>()));
}

seastar::future<> merge_manager::merge(){
    return seastar::do_for_each(boost::make_counting_iterator<size_t>(0), boost::make_counting_iterator<size_t>(seastar::smp::count), [this](auto idx){
        return m.invoke_on(idx, [this, idx](merge_service& merge_s){
            return merge_s.init(get_files_per_merger());
        });
    }).then([this]{
        return m.invoke_on_all([](merge_service& merge_s){
            merge_s.run();
            return merge_s.merge();
        });
    });
}

#pragma once

#include <seastar/core/future.hh>
#include <seastar/core/sharded.hh>
#include <seastar/core/file.hh>
#include <string>
#include <list>
#include <vector>
#include <queue>

struct file_data {
    seastar::file _file;
    uint64_t _read_pos = 0;
    uint64_t _file_size = 0;
    std::string _file_name;
    std::list<std::unique_ptr<char[]> > _data;
};

struct block_data {
    size_t _file_idx;
    std::unique_ptr<char[]> _block;
};

class merge_service{
public:
    merge_service(uint64_t block_size, size_t file_buffer_size, size_t file_buffer_alignment);
    seastar::future<> init(seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > > files);
    seastar::future<> run();
    seastar::future<> merge();
    seastar::future<> clear();
    seastar::future<std::unique_ptr<char[]> > get_next_block(size_t idx);
    seastar::future<> stop();
protected:
    seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > > _files;
    std::vector<file_data> _files_data;
    uint64_t _block_size = 0;
    uint64_t _min_block_count = 0;
    uint64_t _file_chunk_size = 0;
    size_t _file_buffer_size;
    size_t _file_buffer_alignment;
    size_t _output_file_count = 0;
    std::priority_queue<block_data, std::vector<block_data>, std::function<bool(const block_data&, const block_data&)> > _block_list;
};
Compilation (tested with gcc 11.2.0):
c++ main.cc sort-service.cc merge-service.cc merge-manager.cc `pkg-config --cflags --libs --static ../seastar/build/release/seastar.pc` -o sort.out

Parameters:
block size: --bs 1024
input file name: --file ./test_file.out

Notes:
- temporary files are stored in tmp_dir folder
- temporary files can occupy twice as much space as input file
- due to alignment limitation of dma_write block size values are limited: on my machine block size should be divisible by 512 (hovewer disk_*_dma_alignment return 4096)
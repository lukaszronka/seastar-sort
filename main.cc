#include <seastar/core/app-template.hh>
#include <seastar/core/future.hh>
#include <seastar/core/sharded.hh>
#include <seastar/core/file.hh>
#include <seastar/core/seastar.hh>
#include <seastar/core/memory.hh>
#include <iostream>
#include <stdexcept>
#include <chrono>
#include "sort-service.hh"
//#include "merge-service.hh"
#include "merge-manager.hh"

seastar::sharded<sort_service> s;
seastar::sharded<merge_service> m;
std::string tmp_dir_name = "./tmp_dir/";
size_t file_buffer_size {1024*1024};
size_t file_buffer_alignment {4*1024};

seastar::future<> sort(std::string file_path, unsigned int block_size) {
    std::cout << "Phase 1 - sorting...\n";
    return seastar::with_file(seastar::open_file_dma(file_path, seastar::open_flags::ro),  [file_path, block_size] (seastar::file& f){
		return f.size().then([file_path, block_size](uint64_t file_size){
            return s.start(file_path, file_size, block_size, seastar::smp::count, file_buffer_size, file_buffer_alignment).then([]{
                return s.invoke_on_all([] (sort_service& sort_s) {
                    return seastar::do_until([&sort_s]{return sort_s.phase1_finished();}, [&sort_s]{
                        return sort_s.sort();
                    });
                });
            });
        });
    }).then([block_size]{
        return s.stop();
    }).then([block_size]{
        std::cout << "Phase 2 - merging...\n";
        return m.start(block_size, file_buffer_size, file_buffer_alignment).then([block_size]{
            return seastar::do_with(merge_manager(tmp_dir_name, block_size, file_buffer_size, file_buffer_alignment), [](auto& mm){
                return mm.prepare_file_list().then([&mm]{
                    return seastar::do_until([&mm]{return mm.file_count()<=1;}, [&mm]{
                        // std::cout << "merge(), file count: " << mm.file_count() << std::endl;
                        return mm.merge().then([&mm]{
                            if(mm.file_count()<=1){
                                return mm.prepare_file_list();
                            }
                            return seastar::make_ready_future<>();
                        });
                    }).then([&mm]{
                        return seastar::rename_file(mm.get_last_file(), "./sorted.bin");
                    });
                });
            });
        });
    }).then([]{
        std::cout << "Done!\n";
        return m.stop();
    });
}

int main(int argc, char** argv) {
    std::cout << "seastar::memory::free_memory(): " << seastar::memory::free_memory() << " seastar::memory::min_free_memory(): " << seastar::memory::min_free_memory() << std::endl;
   	auto start = std::chrono::high_resolution_clock::now();
    seastar::app_template app;
    app.add_options()
        ("block_size,bs", boost::program_options::value<unsigned int>()->default_value(1024), "block size")
        ;
    app.add_positional_options({
        {"file_path", boost::program_options::value<std::string>()->default_value("./test_file.bin"), "file to sort", -1}
    });
    try {
        app.run(argc, argv, [&app]{
            auto& args = app.configuration();
            std::string file_path = args["file_path"].as<std::string>();
            unsigned int block_size = args["block_size"].as<unsigned int>();
            return sort(file_path, block_size);
        });

    } catch(...) {
        std::cerr << "Couldn't start application: " << std::current_exception() << std::endl;
        return 1;
    }
    
    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
    std::cout << "Time: " << time*1e-9 << "s" << std::endl;
    
    return 0;
}
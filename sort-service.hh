#pragma once

#include <seastar/core/future.hh>
#include <iostream>
#include <deque>
#include <memory>

class sort_service {
public:
    sort_service(const std::string& file_path, const uint64_t file_size, const uint64_t block_size, const size_t shard_count, size_t file_buffer_size, size_t file_buffer_alignment);
    seastar::future<> sort();
    bool phase1_finished();
    seastar::future<> stop();
   
protected:
    seastar::future<> prepare_tmp_dir();
    seastar::future<> read_data_from_file();
    seastar::future<> sort_data();
    seastar::future<> write_data_to_file();
    std::string _file_path;
    uint64_t _block_size = 0;
    uint64_t _read_pos = 0;
    uint64_t _write_pos = 0;
    uint64_t _read_step = 0;
    uint64_t _file_size = 0;
    size_t _file_chunk_size = 0;
    size_t _shard_count;
    size_t _file_buffer_size;
    size_t _file_buffer_alignment;
    uint64_t _file_no = 0;
    std::deque<std::unique_ptr<char[]> > _data;
};

#pragma once

#include "merge-service.hh"
#include <seastar/core/future.hh>
#include <seastar/core/sharded.hh>
#include <queue>
#include <vector>
#include <list>

extern seastar::sharded<merge_service> m;


class merge_manager{
public:
    merge_manager(std::string& tmp_dir_name, uint64_t block_size, size_t file_buffer_size, size_t file_buffer_alignment);
    seastar::future<> prepare_file_list();
    std::string get_last_file();
    size_t file_count();
    seastar::future<> merge();
protected:
    seastar::foreign_ptr<std::unique_ptr<std::vector<std::string> > > get_files_per_merger();
    std::string _tmp_dir_name;
    size_t _files_per_service = 2;
    size_t _file_count {0};
    size_t _file_buffer_size;
    size_t _file_buffer_alignment;
    uint64_t _block_size;
    uint64_t _file_chunk_size;
    std::list<std::vector<std::string> > _splitted_files;
};

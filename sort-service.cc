#include "sort-service.hh"
#include <seastar/core/sharded.hh>
#include <seastar/core/memory.hh>
#include <seastar/core/file.hh>
#include <seastar/util/tmp_file.hh>
#include <seastar/core/seastar.hh>
#include <chrono>
#include <algorithm>
#include <cstring>
#include <strstream>
#include <queue>
#include <vector>
#include <functional>

extern std::string tmp_dir_name;

sort_service::sort_service(const std::string& file_path, const uint64_t file_size, const uint64_t block_size, const size_t shard_count, size_t file_buffer_size, size_t file_buffer_alignment)
        : _file_path(file_path), _file_size(file_size), _block_size(block_size), _shard_count(shard_count), _file_buffer_size(file_buffer_size), _file_buffer_alignment(file_buffer_alignment) {
    _file_chunk_size = (file_buffer_size/block_size)*_block_size;
    _read_step = _file_chunk_size * shard_count;
    _read_pos = _file_chunk_size * seastar::this_shard_id();
}

seastar::future<> sort_service::sort() {
    return prepare_tmp_dir().then([this] {
        return read_data_from_file().then([this] {
            return sort_data().then([this](){
                return write_data_to_file().then([]{
                    return seastar::make_ready_future<>();
                });
            });
        });
    });
}

bool sort_service::phase1_finished() {
    return _read_pos >= _file_size;
}

seastar::future<> sort_service::stop() {
    return seastar::make_ready_future<>();
}

seastar::future<> sort_service::prepare_tmp_dir(){
    return seastar::touch_directory(tmp_dir_name).then([]{
        return seastar::make_ready_future<>();
    });
}

seastar::future<> sort_service::read_data_from_file() {
    return seastar::with_file(seastar::open_file_dma(_file_path, seastar::open_flags::ro), [this] (seastar::file& f) {
        return seastar::do_with(seastar::temporary_buffer<char>::aligned(_file_buffer_alignment, _file_buffer_size), [this, &f](auto& buf) {
            return seastar::do_until([this] {return _read_pos >= _file_size || seastar::memory::free_memory() < (seastar::memory::min_free_memory()/4);}, [this, &f, &buf]{
                return f.dma_read(_read_pos, buf.get_write(), _file_chunk_size).then([this, &buf] (size_t count) {
                    return seastar::do_with(uint64_t(0),[this, &buf, count](auto& chunk_pos){
                        return seastar::do_until([this, &chunk_pos, count]{return (chunk_pos + _block_size) > count;}, [this, &chunk_pos, count, &buf]{
                            std::unique_ptr<char[]> d = std::make_unique<char[]>(_block_size);
                            std::memcpy(d.get(), buf.get()+chunk_pos, _block_size);
                            _data.push_back(std::move(d));
                            chunk_pos += _block_size;
                            return seastar::make_ready_future<>();
                        });
                    });
                }).then([this]{
                    _read_pos += _read_step;
                    return seastar::make_ready_future<>();
                });
            });
        });
    });
}

seastar::future<> sort_service::sort_data() {
    std::sort(_data.begin(), _data.end(), [this](auto& a, auto& b) {
        return (std::memcmp(a.get(), b.get(), _block_size) < 0);
    });
    return seastar::make_ready_future<>();
}

seastar::future<> sort_service::write_data_to_file() {
    _write_pos = 0;
    return seastar::with_file(seastar::open_file_dma( tmp_dir_name + std::to_string(seastar::this_shard_id()) + "_" + std::to_string(_file_no) + ".tmp",seastar::open_flags::rw | seastar::open_flags::create | seastar::open_flags::truncate), [this] (seastar::file& f){
        return seastar::do_with(std::make_unique<char[]>(_file_buffer_size), uint64_t(0), [this, &f](auto& buf, auto& chunk_pos){
            return seastar::do_for_each(_data, [this, &f, &buf, &chunk_pos] (auto& d) {
                std::memcpy(buf.get()+chunk_pos, d.get(), _block_size);
                chunk_pos += _block_size;
                if(chunk_pos > (_file_buffer_size-_block_size))
                {
                    return f.dma_write(_write_pos, buf.get(), _file_chunk_size).then([this, &chunk_pos](size_t count) {
                        chunk_pos = 0;
                        _write_pos += _file_chunk_size;
                        return seastar::make_ready_future<>();
                    });
                }
                return seastar::make_ready_future<>();
            }).then([this, &f, &buf, &chunk_pos]{
                //write last chunk of data (if it was not full)
                if(chunk_pos > 0)
                {
                    return f.dma_write(_write_pos, buf.get(), chunk_pos).then([](size_t count){
                        return seastar::make_ready_future<>();
                    });
                }
                return seastar::make_ready_future<>();
            });            
        });
    }).then([this]{
        _file_no += 1;
        _data.clear();
        return seastar::make_ready_future<>();
    });
}
